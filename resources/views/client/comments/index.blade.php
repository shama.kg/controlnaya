
@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col">
            @foreach($comments as $comment )
                <div class="border m-2 p-2" >
                    <p>{{$comment->user->name}}</p>
                    <p>{{$comment->comment}}</p>
                    @if(!$comment->approve)
                        <p class="text-danger">Не потвержденный</p>
                    @endif
                    <a class="btn btn-warning" href="{{route('comment.edit', ['comment'=> $comment])}}">edit</a>
                    <form action="{{route('comment.destroy', ['comment'=> $comment])}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button type="submit">delete</button>
                    </form>
                </div>
            @endforeach
        </div>
    </div>
@endsection
