
@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col">
            <form action="{{route('comment.update', ['comment'=>$comment])}}" method="post">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Comment</label>
                        <input name="comment" value="{{$comment->comment}}" type="text" class="form-control" id="exampleInputPassword1">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">author</label>
                        <input name="user_id" value="{{$comment->user_id}}" type="text" class="form-control" id="exampleInputPassword1">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">paper_id</label>
                        <input name="paper_id" value="{{$comment->paper_id}}" type="text" class="form-control" id="exampleInputPassword1">
                    </div>

                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">paper_id</label>
                        <input name="approve" value="{{$comment->approve}}" type="text" class="form-control" id="exampleInputPassword1">
                    </div>

                    <button type="submit">edit</button>
            </form>
        </div>
    </div>
@endsection
