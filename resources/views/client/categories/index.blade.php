@extends('layouts.app')
@section('content')

    @can('create-paper', Auth::user())
        <a class="btn btn-success m-2" href="{{route('paper.index')}}">Создать новость</a>
    @endcan
    @can('create-date', Auth::user())
        <a class="btn btn-success m-2" href="{{route('comment.index')}}">Все коментарии</a>
    @endcan
    <div class="dropdown">

        <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
            Категории
        </button>

        <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="{{route('category.index')}}">Все</a>
            @foreach($categories as $category)
            <li><a class="dropdown-item" href={{route('category.show', ['category' => $category])}}>{{$category->name}}</li>
            @endforeach
        </ul>
    </div>

    <div class="row">
        @foreach($newspapers as $paper)
            <a class="text-dark text-decoration-none " href="{{route('paper.show', ['paper' => $paper])}}">
                <div class="col-12 border p-2 m-2">
                    <h2>{{$paper->title}}</h2>
                    <p>{{$paper->content}}</p>
                    @if($paper->publish_date == null)
                    <p class="text-danger">(Ожидает)не публиковано дата</p>
                    @endif
                </div>
            </a>
        @endforeach
            <div class="justify-content-md-center p-5">
                <div class="col-md-auto">
                    {{ $newspapers->links() }}
                </div>
            </div>

    </div>
@endsection
