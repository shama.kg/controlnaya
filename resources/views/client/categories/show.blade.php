@extends('layouts.app')
@section('content')
    <div class="dropdown">
        <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
            Категории
        </button>
        <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="{{route('category.index')}}">Все</a>
            @foreach($categories as $category)
                <li><a class="dropdown-item" href={{route('category.show', ['category' => $category])}}>{{$category->name}}</li>
            @endforeach
        </ul>
    </div>

    <div class="row">
        @foreach($newspapers as $paper)
            <a class="text-dark text-decoration-none " href="{{route('paper.show', ['paper' => $paper])}}">
                <div class="col-12 border p-2 m-2">
                    <h2>{{$paper->title}}</h2>
                    <p>{{$paper->content}}</p>
                </div>
            </a>
        @endforeach

@endsection
