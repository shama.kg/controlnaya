@extends('layouts.app')
@section('content')
    @can('create-paper', Auth::user())
        <a class="btn btn-success m-2" href="{{route('paper.index')}}">Создать новость</a>
    @endcan
    <div class="row">
        <div class="col-6 border p-2">
            <h2>{{$paper->title}}</h2>
            <p>{{$paper->content}}</p>
            <p>дата публикации: {{$paper->publish_date}}</p>
            <p>дата создание: {{$paper->created_at}}</p>
            <p>автор: {{$paper->user->name}}</p>
            @can('create-date', auth()->user())
            <form action="{{route('paper.destroy',['paper' => $paper])}}" method="post">
                @csrf
                @method('DELETE')
                <button class="btn btn-danger" type="submit">delete</button>
            </form>
                <a href="{{route('paper.edit', ['paper'=>$paper])}}" class="btn btn-warning">edit</a>
            @endcan

            @can('create-paper', Auth::user())
                <div class="col-6">
                    <form action="{{route('rating-paper',['paper'=>$paper])}}" method="post">
                        @csrf
                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">quality</label>
                            <input name="quality" type="text" class="form-control" id="exampleInputPassword1">
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">relevance</label>
                            <input name="relevance" type="text" class="form-control" id="exampleInputPassword1">
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">satisfaction</label>
                            <input name="satisfaction" type="text" class="form-control" id="exampleInputPassword1">
                        </div>
                        <button class="btn btn-warning" type="submit" >оценить</button>
                    </form>
                </div>
            @endcan
        </div>

        <div class="col-4">
            @auth()
                <form action="{{route('papers.comments.store', ['paper' => $paper])}}" method="post">
                    @csrf
                    <input name="comment" class="comments-input" type="text" placeholder="Добавить комментарии..."/>
                    <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
                    <input type="hidden" name="post_user_id" value="{{$paper->user_id}}">

                    <button type="submit" class="btn btn-info">добавить</button>

                </form>
            @endauth
            <h6><b>Коментарии</b>:</h6>
            @foreach($comments as $comment)
                <p><i>{{$comment->user->name}}</i></p>
                <p>{{$comment->comment}}</p>
                <br>
            @endforeach

        </div>
        </div>




@endsection
