
@extends('layouts.app')
@section('content')

    <h5>Создать </h5>
    <form action="{{route('paper.update',['paper'=> $paper])}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
        </div>
        <div class="form-group">
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Title</label>
                <input name="title" value="{{$paper->title}}" type="text" class="form-control" id="exampleInputPassword1">
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword2" class="form-label">Content</label>
                <input name="content" value="{{$paper->content}}" type="text" class="form-control" id="exampleInputPassword2">
            </div>

            @if(Auth::user()->is_admin)
                <div class="mb-3">
                    <label for="exampleInputPassword2" class="form-label">Дата публикации</label>
                    <input type="date" name="publish_date" value="{{$paper->publish_date}}"  class="form-control" id="exampleInputPassword2">
                </div>
            @endif

            <div class="form-group">
                <label class="my-1 mr-2" for="inlineFormCustomSelectPref"> category</label>
                <select class="custom-select my-1 mr-sm-2" name="category_id" id="inlineFormCustomSelectPref">
                    @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label class="my-1 mr-2" for="inlineFormCustomSelectPref"> Tags</label>
                <select class="custom-select my-1 mr-sm-2" name="tag_id" id="inlineFormCustomSelectPref">
                    @foreach($tags as $tag)
                        <option value="{{$tag->id}}">{{$tag->name}}</option>
                    @endforeach
                </select>
            </div>

        </div>
        <button class=" btn btn-success" type="submit">Загрузить</button>
    </form>
@endsection
