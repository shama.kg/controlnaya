<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Paper;
use Carbon\Carbon;

class CategoryController extends Controller
{
    public function index()
    {
        if(auth()->user()){
            auth()->user()->is_admin?
            $newspapers = Paper::orderBy('created_at', 'desc')->paginate(8):
            $newspapers = Paper::where('publish_date','<',Carbon::now())->paginate(8);
        }else{
            $newspapers = Paper::where('publish_date','<',Carbon::now())->paginate(8);
        }

        $categories = Category::all();
        return view('client.categories.index', compact('newspapers', 'categories'));
    }

    public function show(Category $category)
    {
        $newspapers = Category::find($category->id)->papers->where('publish_date','<',Carbon::now());
        $categories = Category::all();
        return view('client.categories.show', compact('newspapers', 'categories'));
    }

}
