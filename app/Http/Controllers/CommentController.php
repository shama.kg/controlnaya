<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $this->authorize('create-date', auth()->user());
        $comments = Comment::all();
        return view('client.comments.index', compact('comments'));
    }

    /**
     * Show the form for creating a new resource.
     */

    public function edit(Comment $comment)
    {
        return view('client.comments.edit', compact('comment'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Comment $comment)
    {
        $this->authorize('create-date', auth()->user());
        $validated = $request->validate([
            'comment'=> ['min:3', 'required', 'max:2048'],
            'user_id'=> [ 'required', 'max:2048'],
            'paper_id'=> [ 'required', 'max:2048'],
            'approve'=> [ 'nullable'],
        ]);
        $comment->update($validated);
        return redirect()->route('comment.index')->with('success', 'Успешно изменен');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Comment $comment)
    {
        $this->authorize('create-date', auth()->user());
        $comment->delete();
        return redirect()->route('comment.index')->with('success', 'Успешно delete');

    }
}
