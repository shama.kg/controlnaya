<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Comment;
use App\Models\Paper;
use App\Models\Rating;
use App\Models\Tag;
use Illuminate\Http\Request;

class PaperController extends Controller
{

    public function index()
    {
        $this->authorize('create-paper', auth()->user());
        $categories = Category::all();
        $tags = Tag::all();
        return view('client.paper.index', compact('categories', 'tags' ));
    }
    public function store(Request $request)
    {
        $this->authorize('create-paper', auth()->user());
       $validated = $request->validate([
           'title' => ['min:3', 'required', 'max:255'],
           'content' => ['min:3', 'required', 'max:2048'],
           'publish_date' => [ 'nullable'],
           'category_id' => [ 'max:255'],
           'tag_id' => [ 'max:255'],
           'user_id' => [ 'max:255'],
       ]);
       $paper = new Paper($validated);
       $paper->save();

        return redirect()->route('category.index')->with('success', 'Успешно создан');
    }

    public function show(Paper $paper)
    {
        $comments = Paper::find($paper->id)->comments->where('approve', '1');
        return view('client.paper.show', compact('paper','comments' ));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Paper $paper)
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('client.paper.edit', compact('paper','categories', 'tags' ));

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Paper $paper)
    {
        $this->authorize('create-paper', auth()->user());

        $validated = $request->validate([
            'title' => ['min:3', 'required', 'max:255'],
            'content' => ['min:3', 'required', 'max:2048'],
            'publish_date' => [ 'nullable'],
            'category_id' => [ 'max:255'],
            'tag_id' => [ 'max:255'],
            'user_id' => [ 'max:255'],
        ]);
        $paper->update($validated);


        return redirect()->route('category.index')->with('success', 'Успешно изменен');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Paper $paper)
    {
        $this->authorize('create-date', auth()->user());
        $paper->delete();
        return redirect()->route('category.index')->with('success', 'Успешно удален');
    }

    public function rating(Request $request ,Paper $paper)
    {
        $this->authorize('create-paper', auth()->user());
        $validated = $request->validate([
            'quality'=> ['int', 'nullable'],
            'relevance'=> ['int', 'nullable'],
            'satisfaction'=> ['int', 'nullable'],
        ]);
        $validated['user_id'] = $paper->user_id;
        $validated['paper_id'] = $paper->id;
        Rating::create($validated);
        return redirect()->back()->with('success', 'оценена');
    }

}
