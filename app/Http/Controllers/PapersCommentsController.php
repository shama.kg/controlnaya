<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Paper;
use Illuminate\Http\Request;

class PapersCommentsController extends Controller
{

    public function store(Request $request, Paper $paper)
    {

        $this->authorize('create-paper', auth()->user());

        $validated = $request->validate([
            'comment' => 'required', 'min:3', 'max:2048',
            'user_id' => 'required', 'int',
        ]);

        $validated['paper_id'] = $paper->id;
        Comment::create($validated);
        return redirect()->back()->with('success', 'комментарий будет отображен только после апрува администратором');
    }

}
