<?php

namespace App\Http\Controllers;

use App\Models\AccountRating;
use App\Models\Paper;
use App\Models\Rating;
use App\Models\User;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function show($id)
    {
        $account = User::find($id);
        $total = Paper::where('user_id', $id)->count();
        $totalQuality = Rating::where('user_id', $id)->sum('quality');
        $totalRelevance = Rating::where('user_id', $id)->sum('relevance');
        $rating = floor( ($total+$totalQuality+$totalRelevance)/3);


        return view('account.show', compact('account', 'rating'));
    }

}
