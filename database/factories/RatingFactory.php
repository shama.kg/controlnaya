<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Rating>
 */
class RatingFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'quality' => rand(1,100),
            'relevance'=> rand(1,100),
            'satisfaction'=> rand(1,100),
            'user_id' => rand(1,10),
            'paper_id'=> rand(1,20),
        ];
    }
}
