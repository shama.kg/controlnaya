<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Paper>
 */
class PaperFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title' => $this->faker->streetName,
            'content' => $this->faker->text,
            'publish_date' => $this->faker->date(),
            'user_id' => rand(1,3),
            'category_id' => rand(1,3),
            'tag_id' => rand(1,3),
        ];
    }
}
