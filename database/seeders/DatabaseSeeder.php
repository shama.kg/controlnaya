<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Paper;
use App\Models\Rating;
use App\Models\Tag;
use Illuminate\Console\View\Components\Task;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
         \App\Models\User::factory(10)->create();
         Tag::factory()->count(3)->create();
         Category::factory()->count(5)->has(Paper::factory()->count(20))->create();
         Comment::factory()->count(50)->create();
         Rating::factory()->count(100)->create();




    }
}
