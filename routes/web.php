<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/',[\App\Http\Controllers\CategoryController::class, 'index'])->name('house');

Route::resource('category', \App\Http\Controllers\CategoryController::class)->only('index', 'show');
Route::resource('paper', \App\Http\Controllers\PaperController::class);
Route::post('paper.{paper}', [\App\Http\Controllers\PaperController::class, 'rating'])->name('rating-paper');
Route::resource('comment', \App\Http\Controllers\CommentController::class);
Route::resource('account', \App\Http\Controllers\AccountController::class)->only('show');
Route::resource('papers.comments', \App\Http\Controllers\PapersCommentsController::class)->only('store');


Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
